#include <iostream>
#include <string>

using namespace std;


int main()
{
	string textToAnalyze; // creates string 
	// int foo = 0; // omitted unused variable
	int vowels = 0;
	int consonants = 0;
	int digits = 0;
	int spaces = 0;
	int lengthOfStringSubmittedForAnalysis = 0;
	int unknownCharacters = 0;
	int checkSum = 0;
	// int bar = 0; // omitted unused variable

	cout << "Welcome to the CIA code Hunter Program!" << endl;
	cout << "Please type in text to analyze: " << endl;
	getline(cin, textToAnalyze); // takes in what the user inputs

	for (int i = 0; i < textToAnalyze.length(); ++i) // this makes the loop run while the text length is greater than i
	{
		if (textToAnalyze[i] == 'a' || textToAnalyze[i] == 'e' || textToAnalyze[i] == 'i' || // makes sure the code knows the vowels
			textToAnalyze[i] == 'o' || textToAnalyze[i] == 'u' || textToAnalyze[i] == 'A' ||
			textToAnalyze[i] == 'E' || textToAnalyze[i] == 'I' || textToAnalyze[i] == 'O' ||
			textToAnalyze[i] == 'U')
		{
			++vowels; // made this ++ instead of --
		}
		else if ((textToAnalyze[i] >= 'a' && textToAnalyze[i] <= 'z') || (textToAnalyze[i] >= 'A' && textToAnalyze[i] <= 'Z')) // checks for all the characters
		{
			++consonants; // Uncommented the code that is needed
		}
		else if (textToAnalyze[i] >= '0' && textToAnalyze[i] <= '9') // checks all of the digits
		{
			++digits;
		}
		else if (textToAnalyze[i] == ' ') // checks for spaces
		{
			++spaces;
		}
		else // if nothing falls into the checks above it is unknown
		{
			++unknownCharacters; 
		}
	}

	lengthOfStringSubmittedForAnalysis = textToAnalyze.length(); // sets the length of the whole string to the correct length 
	checkSum = unknownCharacters + vowels + consonants + digits + spaces; // sets the total number of everything

	cout << "Vowels: " << vowels << endl;
	cout << "Consonants: " << consonants << endl;
	cout << "Digits: " << digits << endl;
	cout << "White spaces: " << spaces << endl;
	cout << "Length of string submitted for analysis: " << lengthOfStringSubmittedForAnalysis << endl;
	cout << "Number of characters CodeHunter could not identify: " << unknownCharacters << endl;
	cout << "Checksum: " << checkSum << endl;

	if (checkSum == lengthOfStringSubmittedForAnalysis) // checks if the numbers to match up so that they don't have any problems
	{
		cout << "This program self checking has found this Analysis to be valid." << endl;
	}
	else
	{
		cout << "WARNING! *** This program self checking has found this Analysis to be invalid! Do not use this data!" << endl;
	}

	system("pause");

	return 0;
}